const Coin = {
    blockTargetSeconds: 60,
    coinUnits: 1000000,
    decimals: 6,
    coinName: 'Galaxia',
    coinTicker: 'GLX',
    totalSupply: 55000000000000,
    maxBlockHeight: 500000000,
    heightIndexOffset: 0
};

export { Coin };
